var elixir = require('laravel-elixir');
require('laravel-elixir-vueify');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix
        .sass('style.scss')
        .copy('node_modules/foundation-sites/js', 'resources/assets/js/foundation')
        .copy('node_modules/font-awesome/fonts', 'public/fonts')
        .copy('resources/assets/fonts', 'public/fonts')
        .copy('resources/assets/js/helpers.js', 'public/js')
        //.copy('node_modules/foundation-sites/js/foundation.util.triggers.js', 'resources/assets/js/foundation')
        //.copy('node_modules/foundation-sites/js/foundation.util.mediaQuery.js', 'resources/assets/js/foundation')
        //.copy('node_modules/foundation-sites/js/foundation.sticky.js', 'resources/assets/js/foundation')
        //.copy('node_modules/foundation-sites/js/foundation.magellan.js', 'resources/assets/js/foundation')
        .scripts([
            'jquery.js',
            'foundation/foundation.core.js',
            'foundation/foundation.util.mediaQuery.js',
            'foundation/foundation.util.triggers.js',
            'foundation/foundation.util.keyboard.js',
            'foundation/foundation.util.box.js',
            'foundation/foundation.util.nest.js',
            'foundation/foundation.sticky.js',
            'foundation/foundation.magellan.js',
            'foundation/foundation.dropdownMenu.js',
            'jquery.plugin.js',
            'jquery.countdown.js',
            'autocomplete.js',

            'helpers.js'
        ]);
    mix.browserify('rsvp.js');
});
