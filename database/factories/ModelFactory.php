<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Member::class, function (Faker\Generator $faker) {
    return [
        'id' => $faker->uuid,
        'name' => $faker->name,
        'type' => $faker->randomElement(['adult-inv', 'adult-p1', 'child']),
        'party_id' => function () {
            return factory(App\Party::class)->create()->id;
        },
        'rsvp_response' => $faker->randomElement([null, 'confirmed', 'regret']),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'responded_at' => $faker->dateTime,
    ];
});

$factory->define(App\Party::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'email' => $faker->randomElement([$faker->email, null]),
        'public_notes' => $faker->sentences(3, true),
        'private_notes' => $faker->sentences(3, true),
        'allow_plus_one' => true,
        'recieved_std' => $faker->boolean(),
        'recieved_invite' => $faker->boolean(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'responded_at' => $faker->dateTime,
    ];
});

