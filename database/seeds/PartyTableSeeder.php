<?php

use Illuminate\Database\Seeder;
use League\Csv\Reader;

class PartyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!ini_get("auto_detect_line_endings")) {
            ini_set("auto_detect_line_endings", '1');
        }

        foreach(Reader::createFromPath(database_path('seeds/files/parties.csv')) as $row)
        {
            \App\Party::create([
                'name' => $row[0],
                'recieved_std' => !empty($row[4]),
            ]);
        }
        return;

        $party = new \App\Party();

        $party->name = 'Brehm Party';
        $party->phone = '555-5606';
        $party->email = 'email@fff.com';
        $party->address = '1234 address';
        $party->public_notes = 'public notes';
        $party->private_notes = 'protected notes';

        $party->save();

        $party = new \App\Party();

        $party->name = 'Johnson Party';
        $party->phone = '209-742-6961';
        $party->email = 'gary@sti.net';
        $party->address = '4898 Oak Ridge Road';
        $party->public_notes = 'Nancy has a glutin allergy';
        $party->private_notes = 'the parents of the groom';

        $party->save();

        $party = new \App\Party();

        $party->name = 'Bump Party';
        $party->phone = '567-555-1234';
        $party->email = 'brian@bump.com';
        $party->address = 'Address';
        $party->public_notes = 'Brian has a shellfish allergy';
        $party->private_notes = 'groomsman';

        $party->save();
    }
}
