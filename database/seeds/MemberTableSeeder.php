<?php

use App\Party;
use Illuminate\Database\Seeder;
use League\Csv\Reader;

class MemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!ini_get("auto_detect_line_endings")) {
            ini_set("auto_detect_line_endings", '1');
        }
        foreach(Reader::createFromPath(database_path('seeds/files/members.csv')) as $row)
        {
            \App\Member::create([
                'id' => \Ramsey\Uuid\Uuid::uuid4(),
                'party_id' => $row[0],
                'name' => $row[1],
                'type' => empty($row[2]) ? 'adult-inv' : 'child'
            ]);
        }
        return;

        $faker = (new \Faker\Factory())->create();

        $party = Party::where('name', 'LIKE', 'Brehm Party')->first();

        $member = new \App\Member(['id' => $faker->uuid]);
        $member->name = 'Chris Brehm';
        $member->type = 'adult-inv';
        $party->members()->save($member);

        $member = new \App\Member(['id' => $faker->uuid]);
        $member->name = 'Kristian Brehm';
        $member->type = 'adult-inv';
//        $party->members()->save($member);

        $member = new \App\Member(['id' => $faker->uuid]);
        $member->name = 'Brynn Brehm';
        $member->type = 'child';
        $member->age = 5;
        $party->members()->save($member);

        $member = new \App\Member(['id' => $faker->uuid]);
        $member->name = 'Harper Brehm';
        $member->type = 'child';
        $member->age = 2;
        $party->members()->save($member);


        $party = Party::where('name', 'LIKE', 'Johnson Party')->first();

        $member = new \App\Member(['id' => $faker->uuid]);
        $member->name = 'Gary Johnson';
        $member->type = 'adult-inv';
        $party->members()->save($member);

        $member = new \App\Member(['id' => $faker->uuid]);
        $member->name = 'Nancy Johnson';
        $member->type = 'adult-inv';
        $party->members()->save($member);

        $party = Party::where('name', 'LIKE', 'Bump Party')->first();

        $member = new \App\Member(['id' => $faker->uuid]);
        $member->name = 'Brian Bump';
        $member->type = 'adult-inv';
        $party->members()->save($member);

    }
}
