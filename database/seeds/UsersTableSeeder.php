<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new \App\User(['name' => 'Mark Johnson', 'email' => 'marklj@gmail.com', 'password' => Hash::make('12345')]);
        $user1->save();
        $user2 = new \App\User(['name' => 'Alexa Ter Horst', 'email' => 'acterhorst@gmail.com', 'password' => Hash::make('12345')]);
        $user2->save();
    }
}
