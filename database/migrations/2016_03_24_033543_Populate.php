<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Seeder;

class Populate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $seeds = [
//            new UsersTableSeeder,
            new PartyTableSeeder,
            new MemberTableSeeder,
        ];

        /** @var Seeder $seed */
        foreach($seeds as $seed)
        {
            $seed->run();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
