@extends('_layouts.site_template')

@section('header_class')
    cannery-row
@stop

@section('header_title')
    Things to Do
@endsection

@section('header_content')
    <div class="row">
        <div class="medium-8 medium-offset-2 columns ">
            <hr>
            <p>Monterey and the surrounding areas have many amazing and fun things to do. Everything from hiking to shopping and checking out historical landmarks, Monterey has something for everyone.</p>
        </div>
    </div>
    <p><a href="#things_to_do_list" id="tdd_jump" class="button primary large">See the list</a></p>
@stop


@section('content')

    <div class="row">
        <div class="medium-offset-2 medium-8 columns">

            <a name="things_to_do_list"></a>
            <h3>Monterey Bay Aquarium</h3>
            <p>
                886 Cannery Row at David Avenue, Monterey, CA 93940 <br>
                (831) 648-4888
            </p>
            <p>
                <a href="http://montereybayaquarium.org" class="button secondary">Website</a>
            </p>
            <hr>

            <h3>17- Mile Drive</h3>
            <p>17 Mile Dr, Pebble Beach, CA 93953, United States</p>
            <hr>

            <h3>Garrapata State Park</h3>
            <p>Big Sur, CA, United States</p>
            <p>
                <a href="http://www.tripadvisor.com/Attraction_Review-g32737-d142806-Reviews-Garrapata_State_Park-Monterey_Monterey_Peninsula_California.html"
                   class="button secondary">Website</a>
            </p>
            <hr>

            <h3>Cannery Row</h3>
            <p>Cannery Row & Wave Streets, Monterey, CA 93940</p>
            <p>831-649-6690</p>
            <hr>

            <h3>Princess Monterey Whale Watching</h3>
            <p>96 Old Fisherman's Wharf #1, Monterey, CA 93940</p>
            <p>831-372-2203</p>
            <p>
                <a href="http://www.montereywhalewatching.com" class="button secondary">Website</a>
            </p>
            <hr>

            <h3>Carmel-by-the-Sea</h3>
            <p>Ocean Ave, Carmel-by-the-Sea, CA, United States</p>
            <p>
                <a href="http://www.carmelcalifornia.org/" class="button secondary">Website</a>
            </p>
            <hr>

            <h3>Carmel Mission Basilica Museum (the Carmel Mission)</h3>
            <p>
                3080 Rio Rd, Carmel-by-the-Sea, CA 93923, United States <br>
                831-624-1271
            </p>
            <p>
                <a href="http://www.carmelmission.org/museum/" class="button secondary">Website</a>
            </p>

            <h3>Monterey Museum of Art – Pacific Street</h3>
            <p>
                559 Pacific St, Monterey, CA 93940, United States <br>
                831.372.5477
            </p>
            <p>
                <a href="http://montereyart.org" class="button secondary">Website</a>
            </p>
            <hr>

            <h3>Adventures By The Sea</h3>
            <p>
                685 Cannery Row #210, Monterey, CA 93940, United States <br>
                831-372-1807
            </p>
            <p>
                Great for individual paddle boarding, kayaks, biking or with an instructor!
            </p>
            <p>
                <a href="http://adventuresbythesea.com/" class="button secondary">Website</a>
            </p>
            <hr>

            <h3>Wine Tasting in Carmel Valley</h3>
            <p>
                <a href="http://www.carmelvalleycalifornia.com/indexwine.html" class="button secondary">Website</a>
            </p>

        </div>
    </div>
    
@endsection