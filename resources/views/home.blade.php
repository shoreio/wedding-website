@extends('_layouts.site_template')

@section('header_class')
    redwoods
@endsection

@section('header_title')
    Alexa &amp; Mark
    <span class="sub">Just Got Married</span>
@endsection

@section('header_content')
    <div class="location">
        Big Sur, CA
    </div>
    <hr>
    <div class="date">
        November 12, 2016
    </div>
    <div class="countdown hide-for-small-only" id="anchor"></div>

    {{--<div>--}}
        {{--<a href="{{ URL::to('/rsvp') }}" class="button rsvp-button large">RSVP Here</a>--}}
    {{--</div>--}}
@endsection

@section('content')
    <div class="row">
        <div class="text-center small-12 medium-8 medium-offset-2 columns">
            <h3>The Wedding</h3>
            <p class="text-center">
                Saturday, November 12, 2016 <br>
                4:00 PM <br>
                Semi-Casual Attire
            </p>
            <hr>
            <h3>Ceremony and Reception</h3>
            <p class="text-center">
                Big Sur River Inn <br>
                Big Sur, 46800 CA-1, CA 93920, United States
            </p>

            <hr>

            <h3>Dinner Menu</h3>
            <h4>Hors d’ouvres</h4>

            <p class="text-center">
                Crab and Avocado Crostini <br>
                Chicken Satay in Peanut Sauce <br>
                Cheese Platter
            </p>

            <h4>Starter</h4>
            <p>River Inn Ceasar Salad</p>

            <h4>Entrée</h4>
            <p>Flat Iron Steak</p>

            <h4>Sides</h4>
            <p>
                Roasted Veggies <br>
                Garlic Mashed Potatoes
            </p>

            <h4>Children's Menu (ages 3-12)</h4>
            <p>
                Chicken Tenders <br>
                French Fries <br>
                Fruit Cup <br>
                Juice
            </p>

            <p><i>For any dietary needs, please elaborate on your online RVSP.</i></p>

        </div>
    </div>

    <br><br>

    <div class="row">
        <div class="small-12 medium-8 medium-offset-2 columns">

            <img src="{{ asset('images/story.jpg') }}" alt="">

            <h3>How they met...</h3>
            <p>Mark and Alexa met in November of 2009 at Shoreline Church in Monterey,CA. Alexa had just started college to go to Cal State, Monterey Bay and Mark had just transferred to the university as well. Over the course of the following two months, they had been spending numerous hours Skype messaging, Facebooking, and texting. The two started as friends for the first two years- going to school, church, and hanging out with mutual friends. In 2011, the two decided friends just wasn't enough and they went out on a first date.</p>
            <p>Mark took Alexa to eat at Peter B's and to walk the pier in Monterey for their first date. It went so well, he took her on another one! They had gone on small adventures to Santa Cruz or Big Sur. They went on big adventures like Savannah,GA and Ireland. Every moment together was a mutual blessing. They had been doting on each other ever since. Then one day, Mark decided dating Alexa just wasn't enough. So he proposed to her.</p>
            <p>Now as the two plan out their wedding, which will publicly display their love and oath to stand by one another, they hope to take their adventure to the next level. They know that no matter what life throws at them, big or small, having one another by their side makes life just a little less scary (and a lot more enjoyable!). The rest is yet to be written.</p>

            <br>
            <p>To be Continued....</p>

        </div>
    </div>
@endsection

@section('js')
    <script>
        $('.countdown').countdown({
            until: new Date(2016, 11 - 1, 12),
            format: 'od'
        });
    </script>
@endsection