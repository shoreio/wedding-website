@extends('_layouts.admin_template')

@section('content')

    <div id="app">
        <div class="row">
            <h2>@{{ party.name }} Party</h2>
            <div class="medium-6 columns" id="member_container">
                <h4>Adults</h4>
                <ul class="member-items">
                    <li v-for="adult in adults">
                        <div class="member-item" v-bind:class="{ confirmed: adult.rsvp_response == 'confirmed', regret: adult.rsvp_response == 'regret' }">
                            <div style="float:left; margin-right: 10px">
                                <i class="fa fa-3x" v-bind:class="{ 'fa-check-circle': adult.rsvp_response == 'confirmed', 'fa-ban': adult.rsvp_response == 'regret', 'fa-question-circle': adult.rsvp_response == null }"></i>
                            </div>
                            <div style="float:left">
                                <div>@{{ adult.name }}</div>
                                <div class="response">
                                    <button @click="openResponseDropdown(adult)" type="button" data-toggle="response-dropdown-@{{ adult.id }}">@{{ adult.rsvp_response | rsvpResponse }} &blacktriangledown;</button>
                                    <div class="dropdown-pane" id="response-dropdown-@{{ adult.id }}" v-f-dropdown-pane="adult.id" data-dropdown>
                                        <strong>Set person as:</strong><br>
                                        <a href="#" class="small expanded button success" @click.prevent="updateConfirmation(adult, 'confirmed')">Confirmed</a>
                                        <a href="#" class="small expanded button alert" @click.prevent="updateConfirmation(adult, 'regret')">Regret</a>
                                        <a href="#" class="small expanded button secondary" @click.prevent="updateConfirmation(adult, null)">Unconfirmed</a>
                                    </div>
                                </div>
                            </div>
                            <div style="float: right; cursor: pointer; font-size: 140%" @click="removeMember(adult)">&times;</div>
                            <div style="clear: both;"></div>
                        </div>
                    </li>
                </ul>
                <div v-if="children.length > 0">
                    <h4>Children</h4>
                    <ul>
                        <li v-for="child in children">
                            @{{ child.name }}
                            <a href="#" style="font-size: 85%;" @click="removeMember(child)">remove</a>
                        </li>
                    </ul>
                </div>
                <div class="add_member callout">
                    <h4>Add Party Member</h4>
                    <input type="text" v-model="new_member_name" placeholder="Person's Name" />
                    <input type="radio" id="inv_type" value="adult-inv" v-model="new_member_type" checked> <label for="inv_type">Adult</label>
                    {{--<input type="radio" id="p1_type" value="adult-p1" v-model="new_member_type"> <label for="p1_type">+1 Adult</label>--}}
                    <input type="radio" id="child_type" value="child" v-model="new_member_type"> <label for="child_type">Child</label>
                    <br>
                    <button class="button secondary" @click="addMember">Add</button>
                </div>
            </div>
            <div class="medium-6 columns">
                <label>Party Name</label>
                <input type="text" v-model="party.name">
                <label>Phone Number</label>
                <input type="text" v-model="party.phone">
                <label>Address</label>
                <input type="text" v-model="party.address">
                <label>E-Mail Address</label>
                <input type="text" v-model="party.email">
                <label>Notes Provided by Party</label>
                <textarea readonly>@{{ party.public_notes }}</textarea>
                <label>Notes (only you can see this)</label>
                <textarea v-model="party.private_notes"></textarea>
                <input type="checkbox" value="1" v-model="party.recieved_std">
                <label class="">Recieved Save the Date</label><br>
                <input type="checkbox" value="1" v-model="party.recieved_invite">
                <label class="">Recieved Invitation</label>

                <hr>

                <button class="button primary" @click="updatePartyDetails(party)">Save Party Details</button>
            </div>

            <div>
                <a href="{{ URL::to('/manage/view-all') }}" class="button secondary">Go Back</a>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script>
        $(document).foundation();
    </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/vue/1.0.19/vue.min.js"></script>
    <script src="{{ asset('js/helpers.js') }}"></script>

    <script>

        var addMember = function(uuid, name, type, party_id) {
            var url = '{{ URL::to('rsvp/members') }}';
            $.post(
                    url,
                    {
                        party_id: party_id,
                        member_id: uuid,
                        name: name,
                        type: type
                    },
                    function(result) {
                        console.log('Added member: '+name+' as '+type);
                    }
            );
        }

        var removeMember = function(uuid) {
            var url = '{{ URL::to('rsvp/members') }}';
            $.ajax({
                method: 'DELETE',
                url: url,
                data: {
                    member_id: uuid
                }
            });
        }

        var updateConfirmation = function(memberUuid, response) {
            var url = '{{ URL::to('rsvp/members') }}/'+memberUuid;

            if(response == null) {
                response = 'unconfirmed';
            }

            $.ajax({
                method: 'PUT',
                url: url,
                data: {
                    rsvp_response: response
                }
            });
        }

        var updatePartyDetails = function(party) {
            console.log(party.recieved_invite);
            var url = '{{ URL::to('rsvp/parties') }}/'+party.id;
            $.ajax({
                method: 'PUT',
                url: url,
                data: {
                    party_object: party
                },
                success: function(data) {
                    console.log(data);
                }
            });
        }

        var party_json = {!! $party !!};

        Vue.filter('showPlusOne', function (value) {
            if(value == 'adult-p1') return '(+1)';
        })

        Vue.filter('rsvpResponse', function (value) {
            if(value == null) return 'unconfirmed';
            if(value == 'confirmed' || value == 'regret') return value;
        })

        var dropdowns = [];
        Vue.directive('f-dropdown-pane', function(id) {
//            console.log($('#response-dropdown-'+id));
//            dropdowns.push(new Foundation.Dropdown());//new Foundation.Dropdown($('#response-dropdown-'+id))
        })

        new Vue({
            el: '#app',
            data: {
                party: party_json,
                new_member_name: '',
                new_member_type: 'adult-inv',
                dropdown: ''
            },
            methods: {
                addMember: function() {
                    if(this.new_member_name != '' && this.new_member_type != '') {

                        var member = {
                            name: this.new_member_name,
                            type: this.new_member_type,
                            party_id: this.party.id,
                            rsvp_response: null,
                            id: guid()
                        };

                        addMember(member.id, member.name, member.type, this.party.id)
                        this.party.members.push(member);

                        this.new_member_name = '';
                        this.new_member_type = 'adult-inv';
                    }
                },
                removeMember: function(member) {
                    if(confirm('Are you sure you want to remove "'+member.name+'"?')) {
                        removeMember(member.id);
                        this.party.members.$remove(member);
                    }
                },
                updateConfirmation: function(member, rsvpResponse) {
                    if(rsvpResponse == 'confirmed' || rsvpResponse == 'regret' || rsvpResponse == null) {
                        member.rsvp_response = rsvpResponse;
                        updateConfirmation(member.id, rsvpResponse)
                    }
                    $('#response-dropdown-'+member.id).foundation('close');
                },
                updatePartyDetails: function(party) {
                    updatePartyDetails(party);
                    console.log('Updated record');
                    window.location = previous_url;
                },
                openResponseDropdown: function(member, e) {
//                    $('.dropdown-pane').each(function(item) {
//                        item.foundation('close');
//                    });
                    var dropdown = new Foundation.Dropdown($('#response-dropdown-'+member.id));
                }
            },
            computed: {
                adults: function() {
                    return this.party.members.filter(function(member) {
                        return member.type == 'adult-inv' || member.type == 'adult-p1'
                    });
                },
                children: function() {
                    return this.party.members.filter(function(member) {
                        return member.type == 'child'
                    });
                }
            }
        })




    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <style>
        .member-item {
            padding: 10px;
            font-weight: bold;
            background: #CCC;
            margin-bottom: 5px;
            color: #FFF;
        }

        .member-item.confirmed {
            background: #3ADB76;
        }

        .member-item.regret {
            background: #EC5840;
            color: #FFF;
        }

        .member-item .response {
            font-size: 75%;
            font-weight: normal;
        }

        .member-items {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        .dropdown-pane {
            color: #222;
        }
    </style>
@endsection