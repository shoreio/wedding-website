@extends('_layouts.admin_template')

@section('content')


    <div class="row">
        <h2>View All Members</h2>

        <table id="all_members" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Age</th>
                <th>Party</th>
                <th></th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Age</th>
                <th>Party</th>
                <th></th>
            </tr>
            </tfoot>
            <tbody>
                @foreach($members as $member)
                <tr>
                    <td>{{ $member->name }}</td>
                    <td>
                        {{ ucwords(convertMemberTypeName($member->type)) }}
                    </td>
                    <td>{{ $member->age }}</td>
                    <td><a href="{{ URL::to('/manage/party/'.$member->party->id) }}">{{ $member->party->name }}</a></td>
                    <td>
                        <span class="
                        @if($member->rsvp_response == 'confirmed')
                            success
                        @elseif($member->rsvp_response == 'regret')
                            alert
                        @else
                            secondary
                        @endif
                         label">{{ $member->rsvp_response ?: 'none' }}</span>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <a href="{{ URL::to('/manage') }}" class="button secondary">Go to front management page</a>
    </div>

@endsection

@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
@endsection

@section('js')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#all_members').DataTable();
        } );
    </script>
@endsection