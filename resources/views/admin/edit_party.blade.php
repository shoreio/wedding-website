@extends('_layouts.admin_template')

@section('content')

    <div id="app">
        <admin-party-setup party-id="{{ $party_id }}"></admin-party-setup>
    </div>
    <div>
        <a href="{{ URL::to('/manage/view-all') }}" class="button secondary">Go Back</a>
    </div>

@endsection

@section('js')
    <script src="{{ asset('js/rsvp.js') }}"></script>
@stop