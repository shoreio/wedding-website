@extends('_layouts.admin_template')

@section('content')


    <div class="row">
        <div class="medium-3 columns">
            <a href="{{ URL::to('manage/add-party') }}" class="button primary expanded">Add New Party</a>
            <a href="{{ URL::to('manage/view-all') }}" class="button expanded">View All Guests</a>
            <a href="{{ URL::to('manage/view-parties') }}" class="button expanded">View All Parties</a>

            <div class="panel">
                <strong>Number of Parties:</strong> {{ $party_count }}<br/>
                <strong>Invited People:</strong> {{ $invited_member_count }}<br/>
                <strong>Confirmed People:</strong> {{ $confirmed_member_count }}
            </div>
        </div>
        <div class="medium-9 columns">
            <h3>Recent RSVPs</h3>
            <table>
                <tr>
                    <th>Person's Name</th>
                    <th>Response</th>
                    <th>Party</th>
                    <th>Responded at</th>
                </tr>
                @foreach($recent_rsvps as $rsvp)
                <tr>
                    <td>{{ $rsvp->name }}</td>
                    <td>
                        <span class="
                        @if($rsvp->rsvp_response == 'confirmed')
                            success
                        @elseif($rsvp->rsvp_response == 'regret')
                            alert
                        @else
                            secondary
                        @endif
                            label">{{ $rsvp->rsvp_response ?: 'unconfirmed' }}</span>
                    </td>
                    <td>
                        <a href="{{ URL::to('/manage/party/'.$rsvp->party->id) }}">{{ $rsvp->party->name }}</a>
                    </td>
                    <td>
                        {{ $rsvp->responded_at->diffForHumans() }}
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>

@endsection