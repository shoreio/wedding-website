@extends('_layouts.admin_template')

@section('content')


    <div class="row">
        <h2>View All Parties</h2>

        <table id="all_parties" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Party Name </th>
                <th>Date Added</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Party Name</th>
                <th>Date Added</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($parties as $party)
                <tr>
                    <td><a href="{{ URL::to('/manage/party/'.$party->id) }}">{{ $party->name }}</a></td>
                    <td>{{ $party->created_at }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <a href="{{ URL::to('/manage') }}" class="button secondary">Go to front management page</a>
    </div>

@endsection

@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
@endsection

@section('js')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#all_parties').DataTable();
        } );
    </script>
@endsection
