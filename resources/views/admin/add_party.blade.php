@extends('_layouts.admin_template')

@section('content')

    <div class="row">
        <h2>Add Party</h2>
        {{ Form::open(['url' => 'rsvp/parties']) }}

            {{ Form::label('Party Name') }}
            {{ Form::text('party_name') }}
            <button class="button">Add</button>

        {{ Form::close() }}
    </div>

@endsection