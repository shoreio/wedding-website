<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>RSVP - {{ $party->name }}</title>
    <link href='//fonts.googleapis.com/css?family=Amatic+SC:400,700|Amiri:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>

<div class="row rsvp-detail">
    <div class="medium-offset-2 medium-8 columns">
        <h1>RSVP - {{ $party->name }}</h1>
        <form action="{{ URL::to('rsvp/'.$party->id) }}" method="POST">
            {{ method_field('PUT') }}
            {{ csrf_field() }}

            <div class="row">
                <p>Please select each person to RSVP for the wedding & reception. Please note any important allergy information in the "Special Dietary Restrictions and Other Notes" box.</p>
                <div class="medium-6 columns">
                    <h2>Adults</h2>
                    <ul id="adult_list">
                        @foreach($party->adults() as $adult)
                            <input type="hidden" name="all_members[]" value="{{ $adult->id }}">
                            <li><input type="checkbox" id="member_{{ $adult->id }}" name="rsvp_members[{{ $adult->id }}]" value="{{ $adult->id }}" {{ $adult->rsvp ? 'checked' : '' }}> <label for="member_{{ $adult->id }}">{{ $adult->name }}</label></li>
                        @endforeach

                    </ul>

                    @if($party->allow_plus_one and (2 > $party->adults()->count()))
                        <div id="add_plus_one_container">

                            <label for="add_plus_one">Add a +1:</label>
                            <div class="input-group">
                                <input type="text" name="add_plus_one" class="input-group-field" id="add_plus_one" value="" placeholder="Guest name">
                                <div class="input-group-button">
                                    <button type="button" id="add_plus_one_submit" class="button primary">Add</button>
                                </div>
                            </div>

                        </div>
                    @endif

                </div>

                <div class="medium-6 columns">

                    <h2>Children</h2>
                    <ul id="child_list">
                        @foreach($party->children() as $child)
                            <input type="hidden" name="all_members[]" value="{{ $child->id }}">
                            <li><input type="checkbox" id="member_{{ $child->id }}" class="checkbox-inline" name="rsvp_members[]" value="{{ $child->id }}" {{ $child->rsvp ? 'checked' : '' }}> <label for="member_{{ $child->id }}">{{ $child->name }}</label> </li>
                        @endforeach
                    </ul>
                    <label for="add_child">Add a child:</label>
                    <div class="input-group">
                        <input type="text" name="add_child" class="input-group-field" id="add_child" value="" placeholder="Child's name">
                        <div class="input-group-button">
                            <button type="button" id="add_child_submit" class="button primary">Add</button>
                        </div>
                    </div>

                </div>
            </div>

            <div class="callout primary">
                <small>If there are more than 2 adult guests in your party, please contact us directly at <a
                            href="mailto:{{ config('wedding.contact_email') }}">{{ config('wedding.contact_email') }}</a>.</small>
            </div>


            <hr>

            <div class="details">

                <div>
                    <label for="phone">Phone Number:</label>
                    <input type="text" name="phone" id="phone" value="{{ $party->phone }}">
                </div>

                <div>
                    <label for="email">Email:</label>
                    <input type="text" name="email" id="email" value="{{ $party->email }}">
                </div>

                <div>
                    <label for="address">Mailing Address:</label>
                    <input type="text" name="address" id="address" value="{{ $party->address }}">
                </div>

                <div>
                    <label for="notes">Special Dietary Restrictions and Other Notes:</label>
                    <textarea name="public_notes" id="notes">{{ $party->public_notes }}</textarea>
                </div>

            </div>

            <button class="button ">Submit RSVP</button>

        </form>

    </div>
</div>

<script src="{{ asset('js/all.js') }}"></script>
<script>
    $('#add_plus_one_submit').on('click', function() {
        if($('#add_plus_one').val() == '') return;
        var member_id = guid();
        addMember(member_id, $('#add_plus_one').val(), 'adult-p1');
        var hidden = '<input type="hidden" name="all_members[]" value="'+member_id+'">';
        var element = '<li><input type="checkbox" id="member_'+member_id+'" name="rsvp_members[]" value="'+member_id+'" checked> <label for="member_'+member_id+'">'+$('#add_plus_one').val()+'</label></li>';
        $('#add_plus_one').val('');
        $('#adult_list').append(hidden);
        $('#adult_list').append(element);
        $('#add_plus_one_container').hide();
    });
    $('#add_child_submit').on('click', function() {
        if($('#add_child').val() == '') return;
        var member_id = guid();
        addMember(member_id, $('#add_child').val(), 'child');
        var element = '<li><input type="checkbox" id="member_'+member_id+'" name="rsvp_members[]" value="'+member_id+'" checked> <label for="member_'+member_id+'">'+$('#add_child').val()+'</label></li>';
        $('#add_child').val('');
        $('#child_list').append(element)
    });

    var addMember = function(uuid, name, type) {
        var url = '{{ URL::to('rsvp/members') }}';
        $.post(
                url,
                {
                    party_id: '{{ $party->id }}',
                    member_id: uuid,
                    name: name,
                    type: type,
                    rsvp: true
                },
                function(result) {
                    console.log('Added member: '+name+' as '+type);
                }
        );
    }



    $('input').on('keydown', function(event) {
        var x = event.which;
        if (x === 13) {
            event.preventDefault();
        }
    });
</script>

</body>
</html>