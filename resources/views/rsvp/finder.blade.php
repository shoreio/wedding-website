<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>RSVP</title>
    <link href='{{ asset('fonts/fonts.css') }}' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
</head>
<body>

<header class="rsvp" id="header" style="height: 250px;">
    @include('_parts.top_navigation')
</header>

<div class="row">

    <div class="medium-8 medium-offset-2 columns text-center">
        <h1>RSVP</h1>
        <h3>Find your party's invitation</h3>
        <h4>Enter your name below to search</h4>
        <input class="member-lookup" id="autocomplete_members" type="text" />
        <br><br>
        <div class="callout primary callout-rsvp">
            <strong>Having problems RSVPing?</strong>
            <p>Send us your RSVP directly by emailing us at <a href="mailto:{{ config('wedding.contact_email') }}">{{ config('wedding.contact_email') }}</a>.</p>
        </div>
        <br><br>
        <br><br>
        <a href="{{ URL::to('/') }}" class="button primary">Back to home page</a>
    </div>
</div>
<script src="{{ asset('js/autocomplete.js') }}"></script>
<script type="text/javascript">
    AutoComplete({
        EmptyMessage: "No person found",
        Url: "/rsvp/members",
        Limit: 10,
        QueryArg: "json&adults&list&q",
        _Select: function(item) {
            window.location = "{{ URL::to('rsvp/member-transfer') }}/"+item.getAttribute("data-autocomplete-value");
        },
    }, "#autocomplete_members");
</script>

</body>
</html>