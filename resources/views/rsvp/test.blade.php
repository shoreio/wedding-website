<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>RSVP - {{ $party->name }}</title>
    <link href='//fonts.googleapis.com/css?family=Amatic+SC:400,700|Amiri:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>

<div class="row">
    <div class="medium-offset-2 medium-8 columns">
        <div id="app">
            <user-party-setup party-id="{{ $party->id }}"></user-party-setup>
        </div>
    </div>
</div>

<div>
    <a href="{{ URL::to('/') }}" class="button secondary">Back to home page</a>
</div>

<script src="{{ asset('js/rsvp.js') }}"></script>

</body>
</html>