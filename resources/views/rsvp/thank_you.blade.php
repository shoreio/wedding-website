<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Thanks for your RSVP!</title>
    <link href='//fonts.googleapis.com/css?family=Amatic+SC:400,700|Amiri:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>

    <div class="row">
        <div class="medium-offset-3 medium-6">
            <p align="center">
                <img src="{{ asset('images/us-ally.jpg') }}" width="300" alt="">
            </p>
            <h1>Thank you!</h1>
            <p align="center">Thanks for your RSVP to our wedding. We cant wait to celebrate with you November 12th!
                <br><a class="button" href="{{ URL::to('/') }}">Back to the wedding home page</a>
            </p>
        </div>
    </div>

</body>
</html>