@extends('_layouts.site_template')

@section('header_class')
    big-sur-river-inn
@endsection

@section('header_title')
    Accommodations
@endsection

@section('header_content')
    <div class="explain">
        Lodging and hotels in Big Sur, Monterey, and surrounding areas
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="small-12 medium-8 medium-offset-2 columns">

            <h3>Big Sur River Inn</h3>
            <p class="text-center">
                <img class="thumbnail" src="{{ asset('images/big-sur-river-inn.jpg') }}" alt="">
            </p>
            <p>
                Big Sur, 46800 CA-1, CA 93920, United States <br>
                (831) 667-2700 <br>
            </p>
            <p>
                To reserve a room, please CALL the Inn and let them know you're attending the ter Horst/Johnson wedding November 12th. We have reserved all the rooms in the Inn for that weekend and the dates will show as unavailable if you search online. Guests must reserve for a minimum of two days (Friday the 11th and Saturday the 12th). These rooms will be on hold until September 2016. There are only 25 rooms so it is based on a first come, first serve basis.            </p>
            <p>
                <b>Room rates are as followed:</b> <br>
                Single Queen: $175/night <br>
                Double Queen: $200/night <br>
                Suite: $300/night <br>
                <small>* prices do not include tax</small>
            </p>
            <p>
                <a href="http://www.bigsurriverinn.com/" target="_blank" class="button secondary">Website</a>
                <a href="info@bigsurriverinn.com" class="button secondary">E-Mail</a>
            </p>

            <hr>

            <h3>The Normandy Inn</h3>
            <p class="text-center">
                <img class="thumbnail" src="{{ asset('images/normandy-inn.jpg') }}" alt="">
            </p>
            <p>
                Ocean Ave, Carmel-by-the-Sea, CA 93923, United States <br>
                831-624-3825
            </p>
            <p>
                I picked the Normandy Inn in Carmel-by-the-sea, but if you have a little extra cash to spend, almost all hotels in Carmel-by-the-sea are conveniently located near shopping, the beach, fantastic and diverse restaurants. It's the classier town on the peninsula. Really great for dinners! This hotel in particular runs rooms for about $250/night.
            </p>
            <p>
                <a href="http://www.normandyinncarmel.com/" target="_blank" class="button secondary">Website</a>
            </p>
            <hr>

            <h3>Mariposa Inn and Suites</h3>
            <p class="text-center">
                <img class="thumbnail" src="{{ asset('images/mariposa-inn.jpg') }}" alt="">
            </p>
            <p>
                1386 Munras Ave, Monterey, CA 93940, United States <br>
                (831) 649-1414 <br>
            </p>
            <p>
                Rooms from $149/night ( $95/night if you're a AAA member).
            </p>
            <p>
                <a href="http://mariposamonterey.com" class="button secondary">Website</a>
                <a class="button secondary" href="mailto:info@mariposamonterey.com">E-Mail</a>
            </p>
            <hr>

            <h3>Lovers Point Inn</h3>
            <p class="text-center">
                <img class="thumbnail" src="{{ asset('images/lovers-point-inn.jpg') }}" alt="">
            </p>
            <p>
                625 Ocean View Blvd, Pacific Grove, CA 93950, United States <br>
                (831) 373-4771 <br>
            </p>
            <p>
                Right off of Lover's Point. Great place for going to the beach, bike rides, or to just check out the Pacific Grove/ Monterey community. Rooms range from $119-$500/night.
            </p>
            <p>
                <a class="button secondary" href="http://loverspointinnpg.com">Website</a>
            </p>
            <hr>

            <h3>Butterfly Grove Inn</h3>
            <p class="text-center">
                <img class="thumbnail" src="{{ asset('images/butterfly-grove-inn.jpg') }}" alt="">
            </p>
            <p>
                1073 Lighthouse Ave, Pacific Grove, CA 93950, United States <br>
                831-250-8191 <br>
            </p>
            <p>
                Definitely an "Inn" instead of a hotel since it's in Pacific Grove which is a more intimate, small, quiet community. It's right next to the ocean though and close to the 17 mile drive. Rooms go for $119/night to about $140/night. It's a 5 minute drive to Cannery Row, a 7 minute drive to downtown Monterey.
            </p>
            <p>
                <a class="button secondary" href="http://butterflygroveinn.com">Website</a>
            </p>
            <hr>

            <h3>The Stevenson Monterey Hotel</h3>
            <p class="text-center">
                <img class="thumbnail" src="{{ asset('images/stevenson-hotel.jpg') }}" alt="">
            </p>
            <p>
                675 Munras Ave, Monterey, CA 93940, United States <br>
                (831) 373-1876 <br>
            </p>
            <p>
                This is a casual hotel. Great if you have family. Not overly glamorous, but it's clean, recommended, and it's got the basics. If you're out and about during your stay and you don't plan on spending much time at the hotel/room, these rooms go for $97-$120/night.
            </p>
            <p>
                <a href="http://thestevensonmonterey.com" class="button secondary">Website</a>
            </p>
            <hr>

            <h3>Bay Park Hotel</h3>
            <p class="text-center">
                <img class="thumbnail" src="{{ asset('images/bay-park-hotel.jpg') }}" alt="">
            </p>
            <p>
                1425 Munras Ave, Monterey, CA 93940, United States <br>
                831-649-1020 <br>
            </p>
            <p>
                Rooms range from $90-$200/night. Right next to the mall and a 5 minute drive to downtown Monterey.
            </p>
            <p>
                <a class="button secondary" href="http://bayparkhotel.com">Website</a>
            </p>
            <hr>

            <h3>Marina Dunes RV Park</h3>
            <p>
                3330 Dunes Dr, Marina, CA 93933, United States
                831-384-6914
            </p>
            <p>
                For our RV people out there! Depending on number of nights and what hook ups you'd need, spaces run for $55-$70/night.
            </p>
            <p>
                <a href="http://marinadunesrv.com" class="button secondary">Website</a>
            </p>
            <hr>

            <h3>Monterey Pines RV Campground</h3>
            <p>
                Higuera Ln, Monterey, CA 93940, United States <br>
                831-656-7563
            </p>
            <p>
                This RV campground exclusive to all active duty, reservists, retirees, family members and DoD civilians. RV vehicle storage is also available. Storage rate is based on the size of the space requested. Call for more information.
            </p>
            <p>
                <a href="http://navylifesw.com/monterey/recreation/recreationallodging/" class="button secondary">Website</a>
            </p>
            <hr>


        </div>
    </div>


    
@endsection