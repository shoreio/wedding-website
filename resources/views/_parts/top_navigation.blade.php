<div data-sticky-container>
    <div data-sticky data-margin-top="0">
        <ul class="vertical medium-horizontal dropdown menu expanded" data-dropdown-menu>
            <li>
                <a href="{{ URL::to('home') }}">The Wedding</a>
            </li>
            <li><a href="{{ URL::to('accommodations') }}">Accommodations</a></li>
            <li><a href="{{ URL::to('things-to-do') }}">Things to Do</a></li>
            {{--                <li><a href="{{ URL::to('photos') }}">Photos</a></li>--}}
            <li><a href="{{ URL::to('registry') }}">Registry</a></li>
        </ul>
    </div>
</div>