@extends('_layouts.admin_template')

@section('content')

    <div class="row">
        <h1>Wedding Management Panel</h1>
        <h3>Please sign in</h3>
    </div>

    <div class="row">
        <div class="medium-4 columns">
            @if($errors->count())
                <div class="callout small warning">
                    Login Failed
                </div>
            @endif
            {{ Form::open(['id'=>'wedding_login']) }}
                <label>Email:</label>
                <input type="text" name="email">
                <label>Password:</label>
                <input type="password" name="password">
                <button type="submit" class="button primary">Login</button>
            {{ Form::close() }}
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function() {

            var store = store || {};
            store.setJWT = function(data) {
                this.JWT = data;
            }

            var previous_url = '{{ \Session::get('_previous.url', '') }}';
            $('#wedding_login').submit(function(e) {
                e.preventDefault();
                console.log('Authenticating JWT');
                $.post('auth/token', $(this).serialize(), function(data) {
                    store.setJWT(data.JWT);
                    window.sessionStorage.accessToken = data.JWT;
                    console.log(data);
                    if(previous_url != '') {
                        window.location = previous_url;
                    }
                    else {
                        window.location = '/manage';
                    }
                }).fail(function() {
                    alert('error logging in');
                });
            });
        });
    </script>
@endsection