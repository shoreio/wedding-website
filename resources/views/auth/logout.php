<h1>Logging out...</h1>

<script>
    if(typeof window.sessionStorage !== 'undefined' && window.sessionStorage.accessToken) {
        console.log('BEFORE: '+window.sessionStorage.accessToken);
        window.sessionStorage.accessToken = null;
        console.log('AFTER: '+window.sessionStorage.accessToken);
    }

//    window.location = '/';
</script>