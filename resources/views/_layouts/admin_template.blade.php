<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Wedding Admin</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Compressed CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/foundation/6.2.0/foundation.min.css">
    @yield('css')
</head>
<body>
    <div class="row">
        <h1>Wedding Website Management</h1>
    </div>

    <hr>
    @yield('content')

    <!-- Compressed JavaScript -->
    <script src="//code.jquery.com/jquery-1.12.2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/foundation/6.2.0/foundation.min.js"></script>

    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });
    </script>

    @yield('js')
</body>
</html>