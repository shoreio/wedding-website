<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Alexa ter Horst &amp; Mark Johnson's Wedding Website | November 12, 2016 | Big Sur, CA</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--<link href='//fonts.googleapis.com/css?family=Amatic+SC:400,700|Amiri:400,700' rel='stylesheet' type='text/css'>--}}
    <link href='{{ asset('fonts/fonts.css') }}' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    @yield('css')
</head>
<body id="anchor">

<header class="@yield('header_class')" id="header">

    @include('_parts.top_navigation')

    <h2 class="title">
        @yield('header_title')
    </h2>

    @yield('header_content')
</header>

<div class="content">

@yield('content')

</div>

<script src="{{ asset('js/all.js') }}"></script>
<script>
    $(document).foundation();
</script>

@yield('js')

</body>
</html>