@extends('_layouts.site_template')

@section('header_class')
big-sur-coast
@stop

@section('header_title')
    Gift Registries
@stop

@section('header_content')
    <div class="row">
        <div class="medium-8 medium-offset-2 columns ">
            <hr>
            <p>What we really want is all of our friends and family together for our big day, but for the traditional gift-giver out there we are registered at a few convenient retailers.</p>
        </div>
    </div>
    <hr>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="medium-4 columns medium-offset-4 text-center">
            <div class="div">
                <a target="_blank" href="http://www.bedbathandbeyond.com/store/giftregistry/view_registry_guest.jsp?pwsToken=&eventType=Wedding&registryId=542648898&wcref=yes&wcsid=277732647">
                    <img src="{{ asset('images/registry/bbab.png') }}" alt="Bed Bath & Beyond" class="thumbnail">
                </a>
                <p>
                    <strong>Bed Bath &amp; Beyond</strong> <br>
                    <a target="_blank" href="http://www.bedbathandbeyond.com/store/giftregistry/view_registry_guest.jsp?pwsToken=&eventType=Wedding&registryId=542648898&wcref=yes&wcsid=277732647" class="button secondary">Website</a>
                </p>
            </div>
            <div>
                <a target="_blank" href="https://www-secure.target.com/gift-registry/giftgiver?registryId=H8g0n7NZMCs-1qAagIIcCA&clkid=d1211b2cN37b42a9e45823a8ebed1b9bf&lnm=81938&afid=The+Knot%2C+Inc.+and+Subsidiaries&ref=tgt_adv_xasd0002">
                    <img src="{{ asset('images/registry/target.jpg') }}" alt="Target" class="thumbnail">
                </a>
                <p>
                    <strong>Target</strong> <br>
                    <a target="_blank" href="https://www-secure.target.com/gift-registry/giftgiver?registryId=H8g0n7NZMCs-1qAagIIcCA&clkid=d1211b2cN37b42a9e45823a8ebed1b9bf&lnm=81938&afid=The+Knot%2C+Inc.+and+Subsidiaries&ref=tgt_adv_xasd0002" class="button secondary">Website</a>
                </p>
            </div>

        </div>
    </div>
    <p><strong>We're still working on our registries! Check back here soon for updates!</strong></p>

@stop

@section('content')
@stop