var Vue = require('vue');
var $ = require('jquery');
var Foundation = require('foundation-sites');

$(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

});

Vue.config.devtools = true

var filter = function(text, length, clamp){
    clamp = clamp || '...';
    var node = document.createElement('div');
    node.innerHTML = text;
    var content = node.textContent;
    return content.length > length ? content.slice(0, length) + clamp : content;
};

Vue.filter('truncate', filter);

Vue.filter('showPlusOne', function (value) {
    if(value == 'adult-p1') return '(+1)';
})

Vue.filter('rsvpResponse', function (value) {
    if(value == null) return 'unconfirmed';
    if(value == 'confirmed' || value == 'regret') return value;
})


import AdminPartySetup from './components/AdminPartySetup.vue';
import UserPartySetup from './components/UserPartySetup.vue';

new Vue({
    el: '#app',

    components: {
        AdminPartySetup,
        UserPartySetup
    },

    ready() {

    },
});