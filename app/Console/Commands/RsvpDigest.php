<?php

namespace App\Console\Commands;

use App\Member;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RsvpDigest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rsvp:digest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Output all updated RSVPs for today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current_date = Carbon::today()->toDateString().'%';
        $parties = \App\Party::where("responded_at", 'LIKE', $current_date)->get();

        $output = $parties->reduce(function($previous_output, $party) {
            $output = $party->name."<br>";
            $output .= $party->members->reduce(function($member_output, $member) {
                return $member_output."• &nbsp;&nbsp;&nbsp;&nbsp;".$member->name . ' - ' . strtoupper($member->rsvp_response) . "<br>";
            }, '');
            $output .= "<br>";
            return $previous_output.$output;
        }, '');

        if(!$output) {
            print 'There are no new RSVP\'s';
            return;
        }

        collect(config('wedding.email_list'))->each(function($email_address) use($output) {
            \Mail::send('emails.rsvp_digest', ['output' => $output], function ($m) use($email_address) {
                $m->from('noreply@markandalexa.us', 'Wedding Website Notifier');

                $m->to($email_address)->subject('New RSVP\'s Have been made!');
            });
        });
        print $output;
    }
}
