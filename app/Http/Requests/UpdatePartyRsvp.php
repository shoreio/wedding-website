<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdatePartyRsvp extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'max:25',
            'email' => 'email|max:40',
            'public_notes' => 'max:10000'
        ];
    }
}
