<?php

Route::group(['middleware' => ['web']], function() {

    Route::post('auth/token', 'Auth\JwtController@loginWithToken');

    Route::get('/', function () {
        return view('home');
    });


    Route::get('home', function () {
        return redirect('/');
    });

    Route::get('accommodations', function ()
    {
        return view('accommodations');
    });

    Route::get('things-to-do', function ()
    {
        return view('things_to_do');
    });


    Route::get('photos', function ()
    {
        return view('photos');
    });

    Route::get('registry', function ()
    {
        return view('registry');
    });


//    Route::get('rsvp', 'RsvpController@find');
//    Route::get('rsvp/members', 'RsvpController@listMembers');
//    Route::get('rsvp/member-transfer/{member_id}', 'RsvpController@transferMemberToParty');
//    Route::get('rsvp/results', 'RsvpController@showResults');
    Route::post('rsvp/parties', 'RsvpController@insertParty');
    Route::put('rsvp/parties/{party_id}', 'RsvpController@updateParty');
//    Route::get('api/rsvp/parties/{party_id}', 'RsvpController@getParty');
//    Route::get('rsvp/{party_id}/thank-you', 'RsvpController@confirm');
//    Route::get('rsvp/{party_id}', 'RsvpController@showParty');
    Route::put('rsvp/{party_id}', 'RsvpController@updatePartyDetails');

    Route::get('login', 'Auth\AuthController@getLogin');
    Route::post('login', 'Auth\AuthController@postLogin');
    Route::get('logout', function () {
        Auth::logout();
        return view('auth.logout');
    });

    Route::group(['middleware' => 'auth'], function() {
        Route::get('manage', 'AdminController@front');
        Route::get('manage/view-all', 'AdminController@viewAll');
        Route::get('manage/view-parties', 'AdminController@viewParties');
        Route::get('manage/add-party', 'AdminController@addParty');
        Route::get('manage/party/{party_id}', 'AdminController@viewParty');

    });
});

Route::post('rsvp/members', 'RsvpController@addMember');
Route::put('rsvp/members/{member_uuid}', 'RsvpController@modifyMember');
Route::delete('rsvp/members', [
//    'middleware' => 'auth',
    'uses' => 'RsvpController@removeMember'
]);


//Route::get('jwt', function ()
//{
//    $secret_key = base64_decode(env('JWT_SECRET_KEY'));
//    $signer = new \Lcobucci\JWT\Signer\Hmac\Sha256;
//
//    $jwt = (new \Lcobucci\JWT\Builder())
//        ->setExpiration(time()+(60*60*24)) // 24 hours
//        ->setIssuer('http://markandalexa.us')
//        ->setIssuedAt(time())
//        ->set('data', [
//            'user_id' => 1,
//            'manage' => true,
//        ])
//        ->sign($signer, $secret_key)
//        ->getToken();
//
//    $parsed = (new \Lcobucci\JWT\Parser())->parse((string) $jwt.'aasdf');
//    $validate = new \Lcobucci\JWT\ValidationData;
//    $validate->setIssuer('http://markandalexa.us');
//
//    dd(
//        $jwt,
//        $jwt->verify($signer, $secret_key),
//        $parsed->validate($validate),
//        $parsed->getClaims()['data']->getValue()->manage
//    );
//});
