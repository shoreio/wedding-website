<?php namespace App\Http\Controllers;

use App\Member;
use App\Party;
use Illuminate\Routing\Controller;

class AdminController extends Controller
{

    public function front()
    {
        $recent_rsvps = Member::with('party')->whereNotNull('responded_at')->where('rsvp_response', '!=', '')->where('rsvp_response', 'NOT LIKE', 'unconfirmed')->orderBy('responded_at', 'DESC')->limit(30)->get();
        $party_count = Party::count();
        $invited_member_count = Member::count();
        $confirmed_member_count = Member::where('rsvp_response', 'LIKE', 'confirmed')->get()->count();

        return view('admin.front')
            ->with(compact('recent_rsvps', 'party_count', 'confirmed_member_count','invited_member_count'));
    }

    public function viewAll()
    {
        $members = Member::with('party')->get();
        return view('admin.view_all')
            ->with(compact('members'));
    }

    public function viewParties()
    {
        $parties = Party::all();
        return view('admin.view_parties')
            ->with(compact('parties'));
    }
    
    public function addParty()
    {
        return view('admin.add_party');
    }

    public function viewParty($party_id)
    {
        $party = Party::with('members')->whereId($party_id)->first();
        return view('admin.edit_party')
            ->with('party_id', $party->id);
    }
    
    public function viewPartyNew($party_id)
    {
        return view('test')
            ->with('party_id', $party_id);
    }

}