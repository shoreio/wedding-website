<?php namespace App\Http\Controllers;

use App\Events\MemberUpdatedRsvpResponse;
use App\Http\Requests\AddMember;
use App\Http\Requests\ModifyMember;
use App\Http\Requests\RemoveMember;
use App\Http\Requests\UpdatePartyRsvp;
use App\Member;
use App\Party;
use Assert\Assertion;
use DateTime;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RsvpController extends Controller
{

    public function find()
    {
        $members = Member::where('type', 'LIKE', 'adult%')->get();
        return view('rsvp.finder')->with(compact('members'));
    }
    
    public function processFind()
    {
        dd('process find');
    }
    
    public function showResults()
    {
        return view('rsvp.results');
    }

    public function showParty($party_id)
    {
        return view('rsvp.test')->with('party', Party::findOrFail($party_id));
    }
    
    public function confirm($party_id)
    {
        $party = Party::findOrFail($party_id);
        if($party->adults()->count() > 0)
        {
            return view('rsvp.thank_you');
        }
        abort(400);
    }

    public function addMember(AddMember $request)
    {
        Assertion::uuid($request->get('id'));
        $party = Party::findOrFail($request->get('party_id'));
        $member = new Member(['id'=>$request->get('id')]);
        $member->name = $request->get('name');
        $member->type = $request->get('type');
        $member->rsvp_response = $request->get('rsvp_response', null);
        if(!is_null($member->rsvp_response)) {
            $member->responded_at = new DateTime;
            $party->responded_at = new DateTime;
        }
        $party->members()->save($member);
        $party->save();

        return $member->id;
    }
    
    public function removeMember(RemoveMember $request)
    {
        $member_id = $request->get('member_id');

        Member::destroy($member_id);
    }

    public function modifyMember($member_uuid, ModifyMember $request)
    {
        $member = Member::findOrFail($member_uuid);
        if($request->has('rsvp_response')) {
            $rsvp = $request->get('rsvp_response') == 'unconfirmed' ? null : $request->get('rsvp_response');
            $member->rsvp_response = $rsvp;
            $member->responded_at = new \DateTime;
            $member->party->update(['responded_at' => new DateTime]);
            event(MemberUpdatedRsvpResponse::class, $member);
        }
        $member->save();
    }

    public function updateParty($party_id, Request $request)
    {
        /** @var Party $party */
        $party = Party::findOrFail($party_id);

        if($request->has('party_object')) {
            $update = $this->normalizeBooleanValues($request->get('party_object'));
            unset($update['members']);
            unset($update['responded_at']);
            $party->update($update);
            return new JsonResponse($party);
        }
    }

    public function getParty($party_id)
    {
        return Party::with('members')->whereId($party_id)->first()->toJson();
    }

    public function insertParty(Request $request)
    {
        $party = Party::create(['name' => $request->get('party_name')]);
        return redirect()->to('/manage/party/'.$party->id);
    }

    public function transferMemberToParty($member_id)
    {
        $member = Member::findOrFail($member_id);
        return redirect()->to("rsvp/{$member->party_id}");
    }
    
    public function listMembers()
    {
        if(array_key_exists("json", $_GET))
        {
            return response()->json($this->jsonMembers());
        }
        throw new NotFoundHttpException;
    }

    private function jsonMembers()
    {
        if(request()->has('q'))
        {
            $builder = Member::where('name', 'LIKE', '%'.request('q').'%');
            if(array_key_exists("adults", $_GET))
            {
                $builder->where('type', 'LIKE', 'adult%');
            }
            elseif(array_key_exists("children", $_GET))
            {
                $builder->where('type', 'LIKE', 'child');
            }

            if(array_key_exists("list", $_GET))
            {
                return $builder->get()->lists('name', 'id');
            }
            return $builder->get();
        }
    }

    private function normalizeBooleanValues($values) {
        $new_values = $values;
        foreach($values as $key => $value) {
            $v = $value;
            if ($value == 'false' || $value == '0') {
                $v = false;
            }
            elseif ($value == 'true' || $value == '1') {
                $v = true;
            }
            if($key == 'allow_plus_one' or $key == 'recieved_std' or $key == 'recieved_invite') {
                $new_values[$key] = $v;
            }
        }
        return $new_values;
    }

}