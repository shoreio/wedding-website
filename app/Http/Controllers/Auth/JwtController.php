<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Session\TokenMismatchException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class JwtController extends Controller
{

    public function loginWithToken()
    {
        $request = request()->all();

        if (\Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
            $token = $this->getToken(\Auth::user());
            return new JsonResponse(['JWT' => (string) $token]);
        }
        return new JsonResponse([], JsonResponse::HTTP_UNAUTHORIZED);
    }

    public function getToken(User $user)
    {
        $secret_key = base64_decode(env('JWT_SECRET_KEY'));
        $signer = new \Lcobucci\JWT\Signer\Hmac\Sha256;

        $jwt = (new \Lcobucci\JWT\Builder())
            ->setExpiration(time()+(60*60*24)) // 24 hours
            ->setIssuer($_SERVER['HTTP_HOST'])
            ->setIssuedAt(time())
            ->set('data', [
                'user_id' => $user->id,
                'manage' => true,
            ])
            ->sign($signer, $secret_key)
            ->getToken();

        return $jwt;
    }

    private function checkCSRF($token)
    {
        if(csrf_token() !== $token)
        {
            throw new TokenMismatchException;
        }
    }

}