<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Member extends Model
{
    public $incrementing = false;
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'responded_at'];

    function party()
    {
        return $this->belongsTo(Party::class);
    }
    
    public static function updateRsvp($members = null)
    {
        if(is_array($members) or $members instanceof Collection)
        {
            array_walk($members, function($member) {
                Member::updateRsvp($member);
            });
            return;
        }
        if($members instanceof Member)
        {
            if( ! $members->rsvp) {
                $members->rsvp = true;
                $members->save();
            }
            return;
        }
        throw new \Exception('Invalid type');
    }

    public function getTypeAttribute()
    {
        return $this->attributes['type'];
    }
}