<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Party extends Model
{
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'responded_at'];

    public function members()
    {
        return $this->hasMany(Member::class);
    }

    public function children()
    {
        return $this->members->filter(function($member) {
            return $member->type == 'child';
        });
    }

    public function adults()
    {
        return $this->members->filter(function($member) {
            return str_contains($member->type, 'adult');
        });
    }

    public function setRecievedInviteAttribute($value)
    {
        $this->attributes['recieved_invite'] = ($value === 'true' or $value === 1 or $value === '1' or $value === true);
    }

    public function setRecievedStdAttribute($value)
    {
        $this->attributes['recieved_std'] = ($value === 'true' or $value === 1 or $value === '1' or $value === true);
    }

    public function getRecievedStdAttribute($value)
    {
        return (boolean) $value;
    }

    public function getRecievedInviteAttribute($value)
    {
        return (boolean) $value;
    }
}