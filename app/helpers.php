<?php

function convertMemberTypeName($type)
{
    switch($type) {
        case 'adult-inv':
            return 'invited adult';
        case 'child':
            return 'child';
        case 'adult-p1':
            return 'plus 1';
    }
}